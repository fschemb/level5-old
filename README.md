# Level5 Technical Test

Emoji calculator to demonstrate basic knowledge of PHP.
The app was developed using the lastest version of the Laravel framework as well as the latest version of bootstrap for frontend purposes.
Two routes were implemented, one for the main view and another one to handle the ajax post call to calculate the results and return a json string to the main view.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

To run this app you will need:

- Docker
- Web Browser
- NodeJS

### Installing

From a console terminal run:
```
git clone git@bitbucket.org:fschemb/level5.git
cd level5
composer install
docker-compose up -d
npm run development
cp .env.example .env
```
## Deployment

Once installed, go to to 
```
http://10.5.0.6 
```
and try any combination of operands and operators.
Enjoy. 

## Testing

Unit tests may be found at
```
tests/Feature/CalculatorTest.php
```

## Built With

* [Laravel](https://laravel.com/) - The PHP web framework used
* [Composer](https://getcomposer.org/) - Dependency Management
* [Boostrap](https://getbootstrap.com/) - Front-end component library
* [npm](https://www.npmjs.com/) - Package manager for frontend assets compilation
