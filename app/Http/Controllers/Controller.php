<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('pages/calculator', [
        ]);
    }

    public function calculate(Request $request)
    {
        $input = $request->all();

        if (isset($input['operator'])) {
            switch ($input['operator']) {
                case "alien":
                    $result = $input['operand1'] + $input['operand2'];
                    break;
                case "skull":
                    $result = $input['operand1'] - $input['operand2'];
                    break;
                case "ghost":
                    $result = $input['operand1'] * $input['operand2'];
                    break;
                case "scream":
                    $result = $input['operand1'] / $input['operand2'];
                    break;
            }
            return response()->json(['result' => $result]);
        } else {
            return response()->json(['error' => 'You must post some data']);
        }
    }
}
