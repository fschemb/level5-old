require('./bootstrap');

/**
 *  Submit form
 */

$(".calculator-form").submit(function (e) {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    e.preventDefault();

    console.log($(".operand1").val());
    $.ajax({
        type: 'POST',
        url: 'calculate',
        data: {
            "_token": $('#token').val(),
            operand1: $(".operand1").val(),
            operand2: $(".operand2").val(),
            operator: $(".operator").val(),
        },
        success: function (data) {
            $(".result").css("display", "block");
            $("#result").html(data.result);
        }
    });
});
