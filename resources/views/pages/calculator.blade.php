@extends('layouts.default')
@section('content')
    <div class="jumbotron jumbotron-fluid">

        <div class="calculator-area">
            <form accept-charset="UTF-8" class="calculator-form" id="calculator-form">
                <div class="input-group">
                    <div class="oper">
                        <input class="form-control operand1" type="number" required>

                        <select class="form-control operator" required>
                            <option value=""></option>
                            <option value="alien">&#x1F47D; &nbsp +</option>
                            <option value="skull">&#x1F480; &nbsp -</option>
                            <option value="ghost">&#x1F47B; &nbsp x</option>
                            <option value="scream">&#x1F631; &nbsp /</option>
                        </select>

                        <input class="form-control operand2" type="number" required>
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    </div>
                    <input type="submit" value="Calculate" class="form-control submit-form">
                </div>
            </form>
        </div>
        <div class="result">
            Your result is
            <div id="result"></div>
        </div>
    </div>
@stop