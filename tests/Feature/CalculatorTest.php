<?php
declare(strict_types=1);

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class CalculatorTest extends TestCase
{
    protected $client;
    protected $token;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = new Client([
            'base_uri' => 'http://10.5.0.6'
        ]);
        Session::start();

        $this->token =  csrf_token();// Session::token();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testMainView()
    {
        $response = $this->get('/');

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testAjaxCall()
    {
        $response = $this->get('/');

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testAddition()
    {
        $response = $this->client->post('/calculate', [
            'json' => [
                'operand1' => 1,
                'operand2' => 1,
                'operator' => 'alien',
                '_token' => $this->token
            ]
        ]);

        $data = json_decode($response->getBody(), true);

        $this->assertEquals(2, $data['result']);
    }

    public function testSubtraction()
    {
        $response = $this->client->post('/calculate', [
            'json' => [
                'operand1' => 10,
                'operand2' => 5,
                'operator' => 'skull',
                '_token' => $this->token
            ]
        ]);

        $data = json_decode($response->getBody(), true);

        $this->assertEquals(5, $data['result']);
    }

    public function testMultiplication()
    {
        $response = $this->client->post('/calculate', [
            'json' => [
                'operand1' => 2,
                'operand2' => 2,
                'operator' => 'ghost',
                '_token' => $this->token
            ]
        ]);

        $data = json_decode($response->getBody(), true);

        $this->assertEquals(4, $data['result']);
    }

    public function testDivision()
    {
        $response = $this->client->post('/calculate', [
            'json' => [
                'operand1' => 2,
                'operand2' => 2,
                'operator' => 'scream',
                '_token' => $this->token
            ]
        ]);

        $data = json_decode($response->getBody(), true);

        $this->assertEquals(1, $data['result']);
    }
}
